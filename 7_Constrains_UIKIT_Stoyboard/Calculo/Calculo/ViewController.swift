//
//  ViewController.swift
//  Calculo
//
//  Created by Jorge Teofanes Vicuña Valle on 17/11/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultado: UILabel!
    @IBOutlet weak var resultadoDescuento: UILabel!
    @IBOutlet weak var cantidadtxt: UITextField!
    @IBOutlet weak var porcentajetxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        //Cuando se muestre el teclado
        NotificationCenter.default.addObserver(self, selector: #selector(tecladoUp), name: UIResponder.keyboardWillShowNotification, object: nil)
        //Cuando se esconda el teclado
        NotificationCenter.default.addObserver(self, selector: #selector(tecladoDown), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func tecladoUp(){
        let sizeScreen = UIScreen.main.nativeBounds.height
        if sizeScreen == 1136 {
            if self.view.frame.origin.y == 0 { //queremos modificar la posicion de la vista en el eje y
                self.view.frame.origin.y = -50
            }
        }

    }
    @objc func tecladoDown(){
        if self.view.frame.origin.y != 0 { //queremos modificar la posicion de la vista en el eje y
            self.view.frame.origin.y = 0
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    } //Es para que cuando demos click en al alguna parte vacia, se quite el teclado
    
    @IBAction func calcular(_ sender: UIButton) {
        guard let cantidad = cantidadtxt.text else {
            return
        }
        guard let porcentaje = porcentajetxt.text else {
            return
        }
        let cant = (cantidad as NSString).floatValue //NSString permite hacer operaciones
        let porciento = (porcentaje as NSString).floatValue
        let desc = cant * porciento/100
        let res = cant - desc
        
        resultado.text = "$\(res)"
        resultadoDescuento.text = "$\(desc)"
        self.view.endEditing(true) //quitamos el teclado
    }
    
    @IBAction func limpiar(_ sender: UIButton) {
        cantidadtxt.text = ""
        porcentajetxt.text = ""
        resultado.text = "$0.00"
        resultadoDescuento.text = "$0.00"
    }
}

