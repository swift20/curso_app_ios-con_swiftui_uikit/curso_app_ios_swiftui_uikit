//
//  ViewController.swift
//  HelloUIkit
//
//  Created by Jorge Teofanes Vicuña Valle on 11/11/21.
//

import UIKit

class ViewController: UIViewController {
    
    let hola="Saludo"
    
    
    override func viewDidLoad() { //Todo lo que se coloque en esta parte , va a cargar junto con la vista
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("hola desde viewdidload")
    }

    @IBAction func boton(_ sender: UIButton) {
        let alerta = UIAlertController(title: "Titulo", message: "Mensaje de alerta", preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: "Aceptar", style: .default) { (_) in
            print("aceptar")
        }
        alerta.addAction(ok)
        present(alerta, animated: true, completion: nil)
    }
    
}

