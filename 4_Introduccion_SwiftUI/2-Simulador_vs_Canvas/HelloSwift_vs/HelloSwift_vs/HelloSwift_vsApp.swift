//
//  HelloSwift_vsApp.swift
//  HelloSwift_vs
//
//  Created by Jorge Teofanes Vicuña Valle on 11/11/21.
//

import SwiftUI

@main
struct HelloSwift_vsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
