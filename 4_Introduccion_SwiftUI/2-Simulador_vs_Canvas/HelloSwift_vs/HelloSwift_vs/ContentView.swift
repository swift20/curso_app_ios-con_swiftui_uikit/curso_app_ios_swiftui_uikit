//
//  ContentView.swift
//  HelloSwift_vs
//
//  Created by Jorge Teofanes Vicuña Valle on 11/11/21.
//

import SwiftUI

struct ContentView: View {
    
    let x = "Mi botón"
    
    
    // Con el State se nos permite modificar el valor de un "var" dentro del body
    @State private var show = false
    
    var body: some View {
        
        Button(action: {
            show = true
            print("Hola desde consola") //Desde canva no podemos ver la consola, pero si se puede ver desde el simulador
        }) {
            Text(x)
        }.alert(isPresented: $show, content : { //  el "$" antes del show, es porque usamos una variable de tipo binding
            Alert(title: Text("Titulos"), message: Text("Mensaje Aquí"), dismissButton:.default(Text("Aceptar")))
        })
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
