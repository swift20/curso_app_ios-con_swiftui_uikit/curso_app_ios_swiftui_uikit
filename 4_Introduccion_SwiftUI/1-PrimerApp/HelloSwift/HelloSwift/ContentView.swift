//
//  ContentView.swift
//  HelloSwift
//
//  Created by Jorge Teofanes Vicuña Valle on 11/11/21.
//

import SwiftUI

struct ContentView: View {
    //Variables, funciones o metodos pueden ir arriba o abajo del body, pero no dentro de este.
    
    var nombre="Jorge Vicuna"
    var body: some View {
        // Aqui va solo codigo relacionado al diseño
        //Text("Hola mundo")
        Text(nombre)
            .font(.largeTitle)
            .padding()
    }
}

struct ContentView2: View {
    //Variables, funciones o metodos pueden ir arriba o abajo del body, pero no dentro de este.
    
    var nombre="Ing. Electrónica"
    var body: some View {
        // Aqui va solo codigo relacionado al diseño
        //Text("Hola mundo")
        Text(nombre)
            .font(.largeTitle)
            .foregroundColor(Color.blue)
            .multilineTextAlignment(.center)
            .padding()
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
            ContentView2()
                .previewDevice("iPhone 12 Pro Max")
        }
    }
}
