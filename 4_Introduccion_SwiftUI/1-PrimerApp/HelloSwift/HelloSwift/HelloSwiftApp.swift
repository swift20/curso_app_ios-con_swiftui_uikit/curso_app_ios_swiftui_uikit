//
//  HelloSwiftApp.swift
//  HelloSwift
//
//  Created by Jorge Teofanes Vicuña Valle on 11/11/21.
//

import SwiftUI

@main
struct HelloSwiftApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
