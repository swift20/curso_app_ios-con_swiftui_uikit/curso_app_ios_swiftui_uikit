//
//  Modal.swift
//  NavegacionSwiftUI
//
//  Created by Jorge Teofanes Vicuña Valle on 17/11/21.
//

import SwiftUI

struct Modal: View {
    var body: some View {
        ZStack(){
            Color.blue.edgesIgnoringSafeArea(.all)
            Text("Ventana Modal").font(.largeTitle).foregroundColor(.white).bold()
                
        }
    }
}

//struct Modal_Previews: PreviewProvider {
//    static var previews: some View {
//        Modal()
//    }
//}
