//
//  SegundaVista.swift
//  NavegacionSwiftUI
//
//  Created by Jorge Teofanes Vicuña Valle on 18/11/21.
//

import SwiftUI

struct SegundaVista: View {
    
    var saludo : String
    var nombre : String
    
    var body: some View {
        ZStack{
            Color.blue.edgesIgnoringSafeArea(.horizontal)
            Text(saludo + nombre).font(.largeTitle).bold()
        }.navigationBarTitle("Segunda Vista qp ")
        .navigationBarItems(trailing:
            NavigationLink(
                destination: Tercera_Vista(),
                label: {
                    Image(systemName: "plus")
                })
        )
    }
    
//    SOLUCION CUANDO SE TENIA EL BUG DE IR A OTRA PANTALLA
    
//    @State private var showLink = false
//    var body: some View {
//        ZStack{
//            Color.blue.edgesIgnoringSafeArea(.bottom)
//            Text("Segunda Vista").font(.largeTitle).bold()
//            NavigationLink(
//                destination: Tercera_Vista(),
//                isActive: $showLink,
//                label: {
//                    Text("Navigate").hidden()
//                })
//        }.navigationBarTitle("Segunda Vista qp ")
//        .navigationBarItems(trailing:
//                                Button(action:{
//                                    self.showLink = true
//                                }){
//                                    Image(systemName: "plus")
//                                }
//
//        )
//    }
}



// Ya no usamos el canvas

//struct SegundaVista_Previews: PreviewProvider {
//    static var previews: some View {
//        SegundaVista(saludo: "")
//    }
//}
