//
//  TabViewVista.swift
//  NavegacionSwiftUI
//
//  Created by Jorge Teofanes Vicuña Valle on 18/11/21.
//

import SwiftUI

struct TabViewVista: View {
    var body: some View {
        TabView{ // Siempre debe estar en la pagina principal
            ContentView()
                .tabItem{
                    Text("Inicio")
                    Image(systemName: "house")
                }
            SegundaVista(saludo: "Hola ", nombre: "Jorge")
                .tabItem{
                    Text("Documentos")
                    Image(systemName: "doc")
                }
            Tercera_Vista()
                .tabItem{
                    Text("Ajustes")
                    Image(systemName: "gear")
                }
        }
    }
}

