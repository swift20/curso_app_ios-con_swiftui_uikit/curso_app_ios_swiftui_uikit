//
//  Tercera Vista.swift
//  NavegacionSwiftUI
//
//  Created by Jorge Teofanes Vicuña Valle on 18/11/21.
//

import SwiftUI

struct Tercera_Vista: View {
    var body: some View {
        ZStack{
            Color.red.edgesIgnoringSafeArea(.horizontal)
            Text("Tercera Vista").font(.largeTitle).bold()
        }.navigationBarTitle("Tercera Vista qp ")
    }
}

// Ya no usamos el canvas


//struct Tercera_Vista_Previews: PreviewProvider {
//    static var previews: some View {
//        Tercera_Vista()
//    }
//}
