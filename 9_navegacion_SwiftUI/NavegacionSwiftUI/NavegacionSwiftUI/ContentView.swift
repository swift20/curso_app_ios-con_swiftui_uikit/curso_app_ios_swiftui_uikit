//
//  ContentView.swift
//  NavegacionSwiftUI
//
//  Created by Jorge Teofanes Vicuña Valle on 17/11/21.
//

import SwiftUI

struct ContentView: View {
    let saludo  = "Hola como estas, "
    let hola = "Jorge"

    @State private var show = false
    var body: some View {
        NavigationView {
            VStack{
                Button(action:{
                    show = true
                    
                }){
                    Text("Abrir ventana modal")
                }
                NavigationLink(destination: SegundaVista(saludo: saludo,nombre: hola), label: {
                    Text("Navegar a segunda vista")
                })
            }.sheet(isPresented: $show , content: {
                Modal()
            }).navigationBarTitle("Mi titulo",displayMode: .inline)
        }
    }
}

// Ya no usamos el canvas

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//            .previewDevice("iPhone 12 Pro Max")
//    }
//}
