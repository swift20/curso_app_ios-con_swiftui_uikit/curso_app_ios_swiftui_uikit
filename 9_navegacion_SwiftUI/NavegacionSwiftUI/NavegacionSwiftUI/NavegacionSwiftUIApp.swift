//
//  NavegacionSwiftUIApp.swift
//  NavegacionSwiftUI
//
//  Created by Jorge Teofanes Vicuña Valle on 17/11/21.
//

import SwiftUI

@main
struct NavegacionSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
//            ContentView()
            TabViewVista()
        }
    }
}
