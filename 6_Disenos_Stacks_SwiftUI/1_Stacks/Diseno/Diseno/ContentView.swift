//
//  ContentView.swift
//  Diseno
//
//  Created by Jorge Teofanes Vicuña Valle on 17/11/21.
//

import SwiftUI

//vstack vertical
//hstack horizontal
//zstack fondo
//Las stacks son contenedores

struct ContentView: View {
    
    //valores de entorno o enviromentValues
    
    @Environment(\.horizontalSizeClass) var sizeclass //permite acceder a diferentes clases que nos puedes ayudar a modificar nuestras vistas y realizar diferentes acciones
    
    var body: some View {
        if sizeclass == .compact{
            compactDesign()
        }else{
            regularDesign()
        }
        /*ZStack(alignment:Alignment(horizontal: .leading, vertical: .top)){
            Text("Hello, world!")
                .padding()
            Text("Jorge Vicuna Valle")
                .padding()
        }*/
        /*VStack(alignment:.trailing, spacing: 30){
            Text("Hello, world!")
                .padding()
            Text("Jorge Vicuna Valle")
                .padding()
            Text("Ing. Electrónica")
                .padding()
        }*/
    }
}
struct compactDesign:View{
    
    let numero = "100000000"
    let mensaje = "Hola como estas!"
    
    func sendMessage(){ // Funciona para enviar mensaje
        let sms = "sms:\(numero)&body=\(mensaje)"
        guard let stringSMS = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        UIApplication.shared.open(URL.init(string: stringSMS)! , options: [:], completionHandler: nil)
    }
    
    func sendCall(){ //Funcion para realziar una llamada
        guard let number = URL(string: "tel://\(numero)") else { return }
        UIApplication.shared.open(number)
    }
    
    var body: some View{
        ZStack{
            Color.green.edgesIgnoringSafeArea(.all)
            VStack(){
                Image("icon")
                    .resizable()
                    .frame(width: 100, height: 110, alignment: .center)
                    .clipShape(Circle())
                Image(systemName: "person.crop.circle")
                    .font(.system(size: 100,weight: .bold)).foregroundColor(.white)
                    let texto: Text = Text("Qhipa Pacha Code")
                        .font(.largeTitle)
                        .foregroundColor(.white)
                        .bold()
                    texto
                    texto
                    
                    Text("Calle #123")
                        .foregroundColor(.white)
                        .font(.title).italic()
                    
                    //                    Text("Qhipa Pacha Code")
                    //                        .font(.largeTitle)
                    //                        .foregroundColor(.white)
                    //                        .bold()
                HStack{
                    Button(action:{
                        sendCall()
                    }){
                        Image(systemName: "phone.fill")
                            .modifier(boton(color: Color.blue))
                    }
                    Button(action:{
                        sendMessage()
                    }){
                        Image(systemName: "message.fill")
                            .modifier(boton(color: Color.red))
                    }
                }
            }
        }
    }
}

struct boton : ViewModifier {
    var color: Color
    func body(content:Content) -> some View {
        content
            .padding()
            .background(color)
            .clipShape(Circle())
            .foregroundColor(.white)
            .font(.title)
            
    }
}

struct regularDesign:View{
    
    var body: some View{
        ZStack{
            Color.blue.edgesIgnoringSafeArea(.all)
            HStack(){
                Image("icon")
                    .resizable()
                    .frame(width: 100, height: 110, alignment: .center)
                    .clipShape(Circle())
                VStack(alignment:.leading,spacing:10){
                    
                    let texto: Text = Text("Qhipa Pacha Code")          .font(.largeTitle)
                        .foregroundColor(.white)
                        .bold()
                    texto
                    texto
                    
                    Text("Calle #123")
                        .foregroundColor(.white)
                        .font(.title).italic()
                    
                    
                    //                    Text("Qhipa Pacha Code")
                    //                        .font(.largeTitle)
                    //                        .foregroundColor(.white)
                    //                        .bold()
                    
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iPhone 12 Pro Max")
    }
}
