//
//  DisenoApp.swift
//  Diseno
//
//  Created by Jorge Teofanes Vicuña Valle on 17/11/21.
//

import SwiftUI

@main
struct DisenoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
